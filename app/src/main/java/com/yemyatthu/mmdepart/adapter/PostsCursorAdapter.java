package com.yemyatthu.mmdepart.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.bumptech.glide.Glide;
import com.facebook.Session;
import com.yemyatthu.mmdepart.R;
import com.yemyatthu.mmdepart.data.PostTable;

/**
 * Created by yemyatthu on 12/17/14.
 */
public class PostsCursorAdapter extends CursorAdapter {
  private final Object lock = new Object();
  private Session mSession;
  private String mSource = null;

  public PostsCursorAdapter(Context context, Cursor c, int flags) {
    super(context, c, flags);
  }

  @Override public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
    View view = LayoutInflater.from(context).inflate(R.layout.item, viewGroup, false);

    ViewHolder viewHolder = new ViewHolder(view);
    view.setTag(viewHolder);
    return view;
  }

  @Override public void bindView(View view, Context context, Cursor cursor) {
    mSession = Session.getActiveSession();
    mSource = null;

    ViewHolder viewHolder = (ViewHolder) view.getTag();
    String message = cursor.getString(cursor.getColumnIndexOrThrow(PostTable.COLUMN_MESSAGE));
    String pictureUrl = cursor.getString(cursor.getColumnIndexOrThrow(PostTable.COLUMN_OBJECT_ID));
    if (message != null) {
      viewHolder.mItemTitle.setText(message);
    }
    if (pictureUrl != null && mSession != null) {
      Glide.with(context)
          .load("https://graph.facebook.com/v2.2/"
              + pictureUrl
              + "/"
              + "picture?redirect=true&&access_token="
              + mSession.getAccessToken())
          .placeholder(android.R.drawable.screen_background_light_transparent)
          .into(viewHolder.mItemThumbnail);
    }
  }

  static class ViewHolder {
    @InjectView(R.id.item_thumbnail) ImageView mItemThumbnail;
    @InjectView(R.id.item_title) TextView mItemTitle;

    ViewHolder(View view) {
      ButterKnife.inject(this, view);
    }
  }
}
