package com.yemyatthu.mmdepart.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.yemyatthu.mmdepart.R;

public class PostListActivity extends ActionBarActivity {

  @InjectView(R.id.toolbar) Toolbar mToolbar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    setContentView(R.layout.activity_item_detail);
    ButterKnife.inject(this);
    setSupportActionBar(mToolbar);
    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .add(R.id.container, new PostListFragment())
          .commit();
    }
  }
}
