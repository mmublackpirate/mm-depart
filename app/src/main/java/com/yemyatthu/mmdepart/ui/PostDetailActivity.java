package com.yemyatthu.mmdepart.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.yemyatthu.mmdepart.R;

public class PostDetailActivity extends ActionBarActivity {

  @InjectView(R.id.toolbar) Toolbar mToolbar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_item_detail);
    ButterKnife.inject(this);
    setSupportActionBar(mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction().add(R.id.container,
          PostDetailFragment.getInstance(getIntent().getIntExtra("columnId", 0),
              1)) //if the activity is call, this is a small screen
          .commit();
    }
  }
}
