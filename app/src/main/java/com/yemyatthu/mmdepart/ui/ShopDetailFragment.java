package com.yemyatthu.mmdepart.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.bumptech.glide.Glide;
import com.facebook.Session;
import com.yemyatthu.mmdepart.R;
import com.yemyatthu.mmdepart.model.Cover;
import com.yemyatthu.mmdepart.model.Location;
import com.yemyatthu.mmdepart.model.Shop;
import com.yemyatthu.mmdepart.utility.JsonService;
import com.yemyatthu.mmdepart.utility.NetService;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yemyatthu on 12/5/14.
 */
public class ShopDetailFragment extends Fragment {

  @InjectView(R.id.cover_photo) ImageView mCoverPhoto;
  @InjectView(R.id.shop_title) TextView mShopTitle;
  @InjectView(R.id.detail_descrip) TextView mDetailDescrip;
  @InjectView(R.id.detail_phone) TextView mDetailPhone;
  @InjectView(R.id.detail_address) TextView mDetailAddress;
  private Session mSession;
  private Shop mShop;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mSession = Session.getActiveSession();
    setHasOptionsMenu(true);
    setRetainInstance(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_shop_detail, container, false);
    ButterKnife.inject(this, v);
    if (NetService.isInternetAvailable(getActivity())) {
      mShop = new Shop();
      mShop.setName(
          PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_title", ""));
      mShop.setDescription(PreferenceManager.getDefaultSharedPreferences(getActivity())
          .getString("shop_description", ""));
      mShop.setAbout(
          PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_about", ""));
      mShop.setPhone(
          PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_phone", ""));
      mShop.setLink(
          PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_link", ""));
      Location location = new Location();
      location.setCity(
          PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_city", ""));
      location.setCountry(PreferenceManager.getDefaultSharedPreferences(getActivity())
          .getString("shop_country", ""));
      location.setStreet(PreferenceManager.getDefaultSharedPreferences(getActivity())
          .getString("shop_street", ""));
      mShop.setLocation(location);
      Cover cover = new Cover();
      cover.setCoverId(PreferenceManager.getDefaultSharedPreferences(getActivity())
          .getString("shop_coverId", ""));
      mShop.setCover(cover);
      mShopTitle.setText(mShop.getName());
      mDetailDescrip.setText(mShop.getDescription());
      mDetailPhone.setText(mShop.getPhone());
      mDetailAddress.setText(mShop.getLocation().getStreet()
          + "\n"
          + mShop.getLocation().getCity()
          + ", "
          + mShop.getLocation().getCountry());
      if(mSession!=null){
      Glide.with(getActivity())
          .load("https://graph.facebook.com/v2.2/"
              + mShop.getCover().getCoverId()
              + "/"
              + "picture?redirect=true&&access_token="
              + mSession.getAccessToken())
          .into(mCoverPhoto);}
    } else {
      Toast.makeText(getActivity(), getActivity().getString(R.string.check_internet),
          Toast.LENGTH_LONG).show();

      File file = new File(getActivity().getFilesDir() + "/" + JsonService.SHOP_CACHE);
      if (file.exists()) {
        mShop = JsonService.convertToJavaShop(
            JsonService.loadData(getActivity(), JsonService.SHOP_CACHE));
        if (mShop != null) {
          mShopTitle.setText(mShop.getName());
          mDetailDescrip.setText(mShop.getDescription());
          mDetailPhone.setText(mShop.getPhone());
          mDetailAddress.setText(mShop.getLocation().getStreet());
          Glide.with(getActivity())
              .load("https://graph.facebook.com/v2.2/"
                  + mShop.getCover().getCoverId()
                  + "/"
                  + "picture?redirect=true&&access_token="
                  + mSession.getAccessToken())
              .into(mCoverPhoto);
        }
      }
    }
    return v;
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.shop_menu, menu);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    switch (itemId) {
      case android.R.id.home:
        getActivity().finish();
        return true;
      case R.id.view_item:
        Intent i = new Intent(getActivity(), PostListActivity.class);
        startActivity(i);
        return true;
      case R.id.call_shop:
        //We will find phone nos from string and show them in a dialog
        List<String> phoneNos = new ArrayList<String>();
        Pattern pattern = Patterns.PHONE;
        Matcher matcher = pattern.matcher(mShop.getPhone());
        while (matcher.find()) {
          phoneNos.add(matcher.group());
        }
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setIcon(R.drawable.ic_launcher);

        final ArrayAdapter<String> arrayAdapter =
            new ArrayAdapter<String>(getActivity(), android.R.layout.simple_selectable_list_item);
        arrayAdapter.addAll(phoneNos);
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            String phoneNo = arrayAdapter.getItem(which);
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phoneNo));
            startActivity(callIntent);
          }
        });
        builderSingle.show();
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}
