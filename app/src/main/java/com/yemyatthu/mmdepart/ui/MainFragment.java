package com.yemyatthu.mmdepart.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.yemyatthu.mmdepart.R;
import com.yemyatthu.mmdepart.model.Shop;
import com.yemyatthu.mmdepart.utility.JsonService;
import com.yemyatthu.mmdepart.utility.NetService;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by yemyatthu on 12/5/14.
 */
public class MainFragment extends Fragment {
  @InjectView(R.id.facebook_login) LoginButton authButton;
  @InjectView(R.id.loading_view) ProgressBar loadingView;
  private UiLifecycleHelper uiHelper;
  private Session session;
  private MainActivity mActivity;
  private Session.StatusCallback callback = new Session.StatusCallback() {
    @Override
    public void call(Session session, SessionState state, Exception exception) {
      onSessionStateChange(session, state, exception);
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mActivity = (MainActivity) getActivity();
    try {
      PackageInfo info = getActivity().getPackageManager()
          .getPackageInfo("com.yemyatthu.mmdepart", PackageManager.GET_SIGNATURES);
      for (Signature signature : info.signatures) {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
      }
    } catch (PackageManager.NameNotFoundException e) {

    } catch (NoSuchAlgorithmException e) {

    }
    uiHelper = new UiLifecycleHelper(getActivity(), callback);
    uiHelper.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    session = Session.getActiveSession();
    if (session != null && (session.isOpened() || session.isClosed())) {

      onSessionStateChange(session, session.getState(), null);
    }

    uiHelper.onResume();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    uiHelper.onActivityResult(requestCode, resultCode, data);
    Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);

    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void onPause() {
    super.onPause();
    uiHelper.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    uiHelper.onDestroy();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    uiHelper.onSaveInstanceState(outState);
  }

  @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.inject(this, view);
    authButton.setFragment(this);
    authButton.setReadPermissions(Arrays.asList("public_profile"));
    return view;
  }

  private void onSessionStateChange(Session session, SessionState state, Exception exception) {

    if (state.isOpened()) {
      //If, in case, request take a long time, don't show log out button, only show a loading view
      authButton.setVisibility(View.INVISIBLE);
      loadingView.setVisibility(View.VISIBLE);

      if (NetService.isInternetAvailable(getActivity())) {
        session = Session.getActiveSession();
        Request request = new Request(session, "/KoreaCosmeticsOnlineStore", null, HttpMethod.GET,
            new Request.Callback() {
              public void onCompleted(Response response) {
            /* handle the result */
                //Store the detail of shop for later use across the app
                Shop shop = JsonService.convertToJavaShop(response.getRawResponse());
                SharedPreferences.Editor editor =
                    PreferenceManager.getDefaultSharedPreferences(mActivity).edit();
                editor.putString("shop_title", shop.getName()).commit();
                editor.putString("shop_phone", shop.getPhone()).commit();
                Log.d("coverId", shop.getCover().getCoverId());
                editor.putString("shop_coverId", shop.getCover().getCoverId()).commit();
                editor.putString("shop_city", shop.getLocation().getCity()).commit();
                editor.putString("shop_street", shop.getLocation().getStreet()).commit();
                editor.putString("shop_country", shop.getLocation().getCountry()).commit();
                editor.putString("shop_about", shop.getAbout()).commit();
                editor.putString("shop_description", shop.getDescription()).commit();
                editor.putString("shop_link", shop.getLink()).commit();
              }
            });
        request.executeAsync();
      }
      Intent i = new Intent(getActivity(), PostListActivity.class);
      startActivity(i);
      getActivity().finish();
    } else if (state.isClosed()) {

    }
  }
}
