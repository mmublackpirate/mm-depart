package com.yemyatthu.mmdepart.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.bumptech.glide.Glide;
import com.facebook.Session;
import com.yemyatthu.mmdepart.R;
import com.yemyatthu.mmdepart.data.PostTable;
import com.yemyatthu.mmdepart.provider.PostProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yemyatthu on 12/5/14.
 */
public class PostDetailFragment extends Fragment {
  @InjectView(R.id.detail_image) ImageView mPostPicture;
  @InjectView(R.id.contact_card) LinearLayout mContactCard;
  @InjectView(R.id.detail_descrip) TextView mDetailDescrip;
  private Session session;
  private int mColumnId;
  private Cursor mCursor;
  private ShareActionProvider mShareActionProvider;

  public PostDetailFragment() {

  }

  //type is 1 for small screen, 2 for large screen
  public static PostDetailFragment getInstance(int columnId, int type) {
    Bundle args = new Bundle();
    args.putInt("ARGS", columnId);
    args.putInt("TYPE", type);
    PostDetailFragment postDetailFragment = new PostDetailFragment();
    postDetailFragment.setArguments(args);
    return postDetailFragment;
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    session = Session.getActiveSession();
    mColumnId = getArguments().getInt("ARGS");
    setHasOptionsMenu(true);
    setRetainInstance(true);

    String[] projections = {
        PostTable.COLUMN_MESSAGE, PostTable.COLUMN_LARGE_IMAGE, PostTable.COLUMN_LINK,
        PostTable.COLUMN_OBJECT_ID
    };
    mCursor = getActivity().getContentResolver()
        .query(PostProvider.CONTENT_URI, projections, null, null, null);
    mCursor.moveToFirst();
    //This only need with -1, I've no idea why
    //TODO fix hack
    mCursor.moveToPosition(mColumnId - 1);
  }

  @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable final Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_item_detail, container, false);
    ButterKnife.inject(this, view);
    mDetailDescrip.setText(mCursor.getString(mCursor.getColumnIndex(PostTable.COLUMN_MESSAGE)));
    Glide.with(getActivity())
        .load("https://graph.facebook.com/v2.2/"
            + mCursor.getString(mCursor.getColumnIndex(PostTable.COLUMN_OBJECT_ID))
            + "/"
            + "picture?redirect=true&&access_token="
            + session.getAccessToken())
        .into(mPostPicture);

    //We will find phone nos from string and show them in a dialog
    final List<String> phoneNos = new ArrayList<String>();
    Pattern pattern = Patterns.PHONE;
    Matcher m = pattern.matcher(
        PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("shop_phone", ""));
    while (m.find()) {
      phoneNos.add(m.group());
    }
    mContactCard.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setIcon(R.drawable.ic_launcher);

        final ArrayAdapter<String> arrayAdapter =
            new ArrayAdapter<String>(getActivity(), android.R.layout.simple_selectable_list_item);
        arrayAdapter.addAll(phoneNos);
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            String phoneNo = arrayAdapter.getItem(which);
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            callIntent.setData(Uri.parse("tel:" + phoneNo));
            startActivity(callIntent);
          }
        });
        builderSingle.show();
      }
    });
    return view;
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    //only inflate menu for small screen
    if (getArguments().getInt("TYPE") == 1) {
      inflater.inflate(R.menu.menu_detail, menu);
    }
    MenuItem item = menu.findItem(R.id.menu_item_share);
    mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_SEND);
    intent.setType("text/plain");
    intent.putExtra(Intent.EXTRA_TEXT,
        mCursor.getString(mCursor.getColumnIndex(PostTable.COLUMN_LINK)));

    setShareIntent(intent);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    switch (itemId) {
      case android.R.id.home:
        getActivity().finish();
        return true;
      case R.id.view_shop:
        Intent i = new Intent(getActivity(), ShopDetailActivity.class);
        startActivity(i);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void setShareIntent(Intent shareIntent) {
    if (mShareActionProvider != null) {
      mShareActionProvider.setShareIntent(shareIntent);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
  }
}





