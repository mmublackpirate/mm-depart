package com.yemyatthu.mmdepart.ui;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.yemyatthu.mmdepart.R;
import com.yemyatthu.mmdepart.adapter.PostsCursorAdapter;
import com.yemyatthu.mmdepart.data.PostDbHelper;
import com.yemyatthu.mmdepart.data.PostTable;
import com.yemyatthu.mmdepart.listener.EndlessScrollListener;
import com.yemyatthu.mmdepart.model.Post;
import com.yemyatthu.mmdepart.provider.PostProvider;
import com.yemyatthu.mmdepart.utility.JsonService;
import com.yemyatthu.mmdepart.utility.NetService;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yemyatthu on 12/6/14.
 */
public class PostListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

  private static final String[] mProjection = {
      PostTable.COLUMN_ID, PostTable.COLUMN_MESSAGE, PostTable.COLUMN_LARGE_IMAGE,
      PostTable.COLUMN_OBJECT_ID
  };
  @InjectView(R.id.item_list) ListView mListView;
  private Session mSession;
  private List<Post> mPosts;
  private boolean mTwoPane = false;
  private PostsCursorAdapter mPostsCursorAdapter;
  private String mPage;
  private Bundle params;
  private Cursor mCursor;
  private PostDbHelper mPostDbHelper;
  private SQLiteDatabase mDatabase;
  private int count;
  private Menu mMenu = null;
  private View footerLoadingView;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mSession = Session.getActiveSession();
    setHasOptionsMenu(true);
    setRetainInstance(true);
    mPosts = new ArrayList<>();
    params = new Bundle();
    mPostDbHelper = new PostDbHelper(getActivity());

    //initial cursor instantiation
    mCursor = getActivity().getContentResolver()
        .query(PostProvider.CONTENT_URI, mProjection, null, null, null);

    //Open the database to use in the upgrade method later
    mDatabase = mPostDbHelper.getWritableDatabase();

    //This is to increment the database version everytime user refresh
    count = 0;
    SharedPreferences.Editor editor = getActivity().getPreferences(Activity.MODE_PRIVATE).edit();
    int defaultValue =
        getActivity().getPreferences(Activity.MODE_PRIVATE).getInt("count_key", count);
    ++defaultValue;
    editor.putInt("count_key", defaultValue).commit();
    count = getActivity().getPreferences(Activity.MODE_PRIVATE).getInt("count_key", count);
  }

  @Override public void onResume() {
    super.onResume();
  }

  @Override public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable final Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_item_list, container, false);
    ButterKnife.inject(this, view);
    refreshList();

    if (view.findViewById(R.id.detail_container) != null) {
      mTwoPane = true;
    }

    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        doOnItemClick(i);
      }
    });

    onScrollListener();

    //Get the previous scroll position in orientation change
    //Confusingly, it will only work if I use listview.post

    if (savedInstanceState != null) {
      mListView.post(new Runnable() {
        @Override public void run() {
          mListView.setSelection(savedInstanceState.getInt("position"));
        }
      });
    }

    footerLoadingView =
        getActivity().getLayoutInflater().inflate(R.layout.loading_view, null, false);
    return view;
  }

  private void updateDatabase(Post post) {
    ContentValues contentValues = new ContentValues();
    contentValues.put(PostTable.COLUMN_MESSAGE, post.getMessage());
    contentValues.put(PostTable.COLUMN_OBJECT_ID, post.getObjectId());
    contentValues.put(PostTable.COLUMN_LINK, post.getLink());
    contentValues.put(PostTable.COLUMN_SMALL_IMAGE, post.getIcon());
    contentValues.put(PostTable.COLUMN_LARGE_IMAGE, post.getPicture());

    getActivity().getContentResolver().insert(PostProvider.CONTENT_URI, contentValues);
  }

  @Override public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {

    return new android.support.v4.content.CursorLoader(getActivity(), PostProvider.CONTENT_URI,
        mProjection, null, null, null);
  }

  @Override public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader,
      Cursor data) {
    mPostsCursorAdapter.swapCursor(data);
  }

  @Override public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
    mPostsCursorAdapter.swapCursor(null);
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    if (!mTwoPane) {
      inflater.inflate(R.menu.menu_list, menu);
      mMenu = menu;
      MenuItem item = mMenu.findItem(R.id.refresh_list);
      if (item.getActionView() == null) {
        item.setActionView(startRefreshAnim());
      }
    } else {
      inflater.inflate(R.menu.tablet_menu, menu);
      if (mMenu == null) {
        MenuItem item = menu.findItem(R.id.refresh_list);
        if (item.getActionView() == null) {
          item.setActionView(startRefreshAnim());
        }
        mMenu = menu;
      }
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {

    int itemId = item.getItemId();
    switch (itemId) {
      case R.id.view_shop:
        Intent i = new Intent(getActivity(), ShopDetailActivity.class);
        startActivity(i);
        return true;
      case R.id.refresh_list:
        if (NetService.isInternetAvailable(getActivity())) {
          if (item.getActionView() == null) {
            item.setActionView(startRefreshAnim());
          }
          refreshList();
        } else {
          Toast.makeText(getActivity(), getActivity().getString(R.string.check_internet),
              Toast.LENGTH_LONG).show();
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void onScrollListener() {
    mListView.setOnScrollListener(new EndlessScrollListener() {
      @Override public void onLoadMore(final int page, int totalItemsCount) {
        if (NetService.isInternetAvailable(getActivity())) {
          if (mSession != null && mSession.isOpened()) {
            if (params != null) {
              new Request(mSession, "/KoreaCosmeticsOnlineStore/posts", params, HttpMethod.GET,
                  new Request.Callback() {
                    public void onCompleted(Response response) {

            /* handle the result */
                      try {

                        JSONObject responseObject = new JSONObject(response.getRawResponse());
                        String datas = responseObject.getJSONArray("data").toString();
                        JSONObject paging = responseObject.getJSONObject("paging");
                        mPage = paging.getString("next");
                        Uri uri = Uri.parse(mPage);
                        params.putString("limit", uri.getQueryParameter("limit"));
                        params.putString("until", uri.getQueryParameter("until"));

                        final List<Post> posts = JsonService.convertToJavaItems(datas);

                        for (Post post : posts) {

                          //We will only take those with objectId (for picture) and message since
                          //We want both picture and message to be shown
                          if (post.getObjectId() != null && post.getMessage() != null) {
                            mPosts.add(post);
                            updateDatabase(post);
                          }
                        }
                      } catch (JSONException e1) {
                        e1.printStackTrace();
                      }
                    }
                  }).executeAsync();
            }
          }
        } else {
          Toast.makeText(getActivity(), getActivity().getString(R.string.check_internet),
              Toast.LENGTH_LONG).show();
          if (mListView.getFooterViewsCount() > 0) {
            mListView.removeFooterView(footerLoadingView);
          }
        }
      }
    });
  }

  private void doOnItemClick(int position) {
    mCursor = getActivity().getContentResolver()
        .query(PostProvider.CONTENT_URI, mProjection, null, null, null);

    mCursor.moveToPosition(position);
    if (mCursor != null && mCursor.moveToPosition(position)) {

      //if not a tablet , start a new activity
      if (!mTwoPane) {
        final Intent intent = new Intent(getActivity(), PostDetailActivity.class);
        try {
          intent.putExtra("columnId", mCursor.getInt(mCursor.getColumnIndex(PostTable.COLUMN_ID)));
        } catch (CursorIndexOutOfBoundsException e) {
          //Sometime user may click the list item before database update
          //In this case, wait a second before firing up detail view
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            @Override public void run() {
              intent.putExtra("columnId",
                  mCursor.getInt(mCursor.getColumnIndex(PostTable.COLUMN_ID)));
            }
          }, 1000);
        }
        getActivity().startActivity(intent);
      } // if a tablet, replace the detail fragment
      else {

        try {
          getActivity().getSupportFragmentManager().beginTransaction().replace(
              R.id.detail_container, PostDetailFragment.getInstance(
                  mCursor.getInt(mCursor.getColumnIndex(PostTable.COLUMN_ID)), 2)) // large screen
              .commit();
        } catch (CursorIndexOutOfBoundsException e) {
          //Sometime user may click the list item before database update
          //In this case, wait a second before firing up detail view
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            @Override public void run() {
              getActivity().getSupportFragmentManager().beginTransaction().replace(
                  R.id.detail_container, PostDetailFragment.getInstance(
                      mCursor.getInt(mCursor.getColumnIndex(PostTable.COLUMN_ID)),
                      2)) //large screen
                  .commit();
            }
          }, 1000);
        }
      }
    }
  }

  public Request callRequest() {
    return new Request(mSession, "/KoreaCosmeticsOnlineStore/posts", null, HttpMethod.GET,
        new Request.Callback() {
          public void onCompleted(Response response) {
            if (response != null) {

        /* handle the result */
              try {

                JSONObject responseObject = new JSONObject(response.getRawResponse());
                String datas = responseObject.getJSONArray("data").toString();
                JSONObject paging = responseObject.getJSONObject("paging");
                mPage = paging.getString("next");
                Uri uri = Uri.parse(mPage);

                //For paging, store limit and until value from Graph api
                params.putString("limit", uri.getQueryParameter("limit"));
                params.putString("until", uri.getQueryParameter("until"));

                List<Post> posts = JsonService.convertToJavaItems(datas);
                for (Post post : posts) {

                  if (post.getObjectId() != null && post.getMessage() != null) {

                    mPosts.add(post);
                    updateDatabase(post);

                    //get the cursor after the database has been updated
                    mCursor = getActivity().getContentResolver()
                        .query(PostProvider.CONTENT_URI, mProjection, null, null, null);

                    //Remove animation after refreshing
                    MenuItem m;
                    if (mMenu != null) {
                      m = mMenu.findItem(R.id.refresh_list);
                      if (m.getActionView() != null) {
                        // Remove the animation.
                        m.getActionView().clearAnimation();
                        m.setActionView(null);
                      }
                    }

                    //Set Detail View only after database is loaded
                    if (mTwoPane) {
                      FragmentManager fm = getActivity().getSupportFragmentManager();

                      //The first post will be selected
                      mCursor.moveToPosition(0);
                      fm.beginTransaction().replace(R.id.detail_container,
                          PostDetailFragment.getInstance(
                              mCursor.getInt(mCursor.getColumnIndex(PostTable.COLUMN_ID)),
                              2))//this is a large screen
                          .commitAllowingStateLoss();
                    }

                    //if ListView don't have footer loading view, add it
                    if (mListView.getFooterViewsCount() == 0) {
                      mListView.addFooterView(footerLoadingView, null, false);
                    }
                  }
                }
              } catch (JSONException e1) {
                e1.printStackTrace();
              }
            }
          }
        });
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    outState.putInt("position", mListView.getFirstVisiblePosition());
    super.onSaveInstanceState(outState);
  }

  public void refreshList() {
    if (NetService.isInternetAvailable(getActivity())) {

      //Clear the database every time app start
      mListView.setSelection(0);
      mPostDbHelper.onUpgrade(mDatabase, count, count + 1);
      if (mSession != null && mSession.isOpened()) {

        Request request = callRequest();
        request.executeAsync();
      }
    } else {
      Toast.makeText(getActivity(), getActivity().getString(R.string.check_internet),
          Toast.LENGTH_LONG).show();
    }

    Handler handler = new Handler();
    handler.post(new Runnable() {

      @Override
      public void run() {
        mPostsCursorAdapter = new PostsCursorAdapter(getActivity(), mCursor, 0);
        getLoaderManager().initLoader(0, null, PostListFragment.this);
        mListView.setAdapter(mPostsCursorAdapter);
      }
    });
  }

  public ImageView startRefreshAnim() {
    LayoutInflater inflater =
        (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_view, null);
    Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh);
    rotation.setRepeatCount(Animation.INFINITE);
    iv.startAnimation(rotation);
    return iv;
  }
}
