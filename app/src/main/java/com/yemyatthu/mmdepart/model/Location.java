package com.yemyatthu.mmdepart.model;

import com.google.gson.annotations.Expose;

public class Location {

  @Expose
  private String city;
  @Expose
  private String country;
  @Expose
  private Double latitude;
  @Expose
  private Double longitude;
  @Expose
  private String street;
  @Expose
  private String zip;

  /**
   * @return The city
   */
  public String getCity() {
    return city;
  }

  /**
   * @param city The city
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * @return The country
   */
  public String getCountry() {
    return country;
  }

  /**
   * @param country The country
   */
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * @return The latitude
   */
  public Double getLatitude() {
    return latitude;
  }

  /**
   * @param latitude The latitude
   */
  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  /**
   * @return The longitude
   */
  public Double getLongitude() {
    return longitude;
  }

  /**
   * @param longitude The longitude
   */
  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  /**
   * @return The street
   */
  public String getStreet() {
    return street;
  }

  /**
   * @param street The street
   */
  public void setStreet(String street) {
    this.street = street;
  }

  /**
   * @return The zip
   */
  public String getZip() {
    return zip;
  }

  /**
   * @param zip The zip
   */
  public void setZip(String zip) {
    this.zip = zip;
  }
}