package com.yemyatthu.mmdepart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class Shop {

  @Expose
  private String id;
  @Expose
  private String about;
  @Expose
  private String category;
  @SerializedName("category_list") @Expose
  private List<CategoryList> categoryList = new ArrayList<CategoryList>();
  @Expose
  private Cover cover;
  private String bigCover;
  @Expose
  private String description;
  @Expose
  private String link;
  @Expose
  private Location location;
  @Expose
  private String name;
  @Expose
  private String phone;
  @Expose
  private String username;
  @Expose
  private String website;

  public String getBigCover() {
    return bigCover;
  }

  public void setBigCover(String bigCover) {
    this.bigCover = bigCover;
  }

  /**
   * @return The id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return The about
   */
  public String getAbout() {
    return about;
  }

  /**
   * @param about The about
   */
  public void setAbout(String about) {
    this.about = about;
  }

  /**
   * @return The category
   */
  public String getCategory() {
    return category;
  }

  /**
   * @param category The category
   */
  public void setCategory(String category) {
    this.category = category;
  }

  /**
   * @return The categoryList
   */
  public List<CategoryList> getCategoryList() {
    return categoryList;
  }

  /**
   * @param categoryList The category_list
   */
  public void setCategoryList(List<CategoryList> categoryList) {
    this.categoryList = categoryList;
  }

  /**
   * @return The cover
   */
  public Cover getCover() {
    return cover;
  }

  /**
   * @param cover The cover
   */
  public void setCover(Cover cover) {
    this.cover = cover;
  }

  /**
   * @return The description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description The description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return The link
   */
  public String getLink() {
    return link;
  }

  /**
   * @param link The link
   */
  public void setLink(String link) {
    this.link = link;
  }

  /**
   * @return The location
   */
  public Location getLocation() {
    return location;
  }

  /**
   * @param location The location
   */
  public void setLocation(Location location) {
    this.location = location;
  }

  /**
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   * @param phone The phone
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * @return The username
   */
  public String getUsername() {
    return username;
  }

  /**
   * @param username The username
   */
  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * @return The website
   */
  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }
}