package com.yemyatthu.mmdepart.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class From {

  @Expose
  private String category;
  @SerializedName("category_list") @Expose
  private List<CategoryList> categoryList = new ArrayList<CategoryList>();
  @Expose
  private String name;
  @Expose
  private String id;

  /**
   * @return The category
   */
  public String getCategory() {
    return category;
  }

  /**
   * @param category The category
   */
  public void setCategory(String category) {
    this.category = category;
  }

  /**
   * @return The categoryList
   */
  public List<CategoryList> getCategoryList() {
    return categoryList;
  }

  /**
   * @param categoryList The category_list
   */
  public void setCategoryList(List<CategoryList> categoryList) {
    this.categoryList = categoryList;
  }

  /**
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(String id) {
    this.id = id;
  }
}