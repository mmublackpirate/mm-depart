package com.yemyatthu.mmdepart.model;

import com.google.gson.annotations.Expose;

/**
 * Created by yemyatthu on 12/5/14.
 */
public class Parking {

  @Expose
  private Integer lot;
  @Expose
  private Integer street;
  @Expose
  private Integer valet;

  /**
   * @return The lot
   */
  public Integer getLot() {
    return lot;
  }

  /**
   * @param lot The lot
   */
  public void setLot(Integer lot) {
    this.lot = lot;
  }

  /**
   * @return The street
   */
  public Integer getStreet() {
    return street;
  }

  /**
   * @param street The street
   */
  public void setStreet(Integer street) {
    this.street = street;
  }

  /**
   * @return The valet
   */
  public Integer getValet() {
    return valet;
  }

  /**
   * @param valet The valet
   */
  public void setValet(Integer valet) {
    this.valet = valet;
  }
}
