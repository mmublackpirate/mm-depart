package com.yemyatthu.mmdepart.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post implements Parcelable {

  public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
    public Post createFromParcel(Parcel source) {
      return new Post(source);
    }

    public Post[] newArray(int size) {
      return new Post[size];
    }
  };
  @Expose
  private String id;
  @Expose
  private From from;
  @Expose
  private String message;
  @Expose
  private String picture;
  @Expose
  private String link;
  @Expose
  private String icon;
  @SerializedName("object_id") @Expose
  private String objectId;

  public Post() {
  }

  private Post(Parcel in) {
    this.id = in.readString();
    this.message = in.readString();
    this.link = in.readString();
    this.objectId = in.readString();
  }

  /**
   * @return The id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return The from
   */
  public From getFrom() {
    return from;
  }

  /**
   * @param from The from
   */
  public void setFrom(From from) {
    this.from = from;
  }

  /**
   * @return The message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message The message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return The picture
   */
  public String getPicture() {
    return picture;
  }

  /**
   * @param picture The picture
   */
  public void setPicture(String picture) {
    this.picture = picture;
  }

  /**
   * @return The link
   */
  public String getLink() {
    return link;
  }

  /**
   * @param link The link
   */
  public void setLink(String link) {
    this.link = link;
  }

  /**
   * @return The icon
   */
  public String getIcon() {
    return icon;
  }

  /**
   * @param icon The icon
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }

  /**
   * @return The objectId
   */
  public String getObjectId() {
    return objectId;
  }

  /**
   * @param objectId The object_id
   */
  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.id);
    dest.writeString(this.message);
    dest.writeString(this.link);
    dest.writeString(this.objectId);
  }
}