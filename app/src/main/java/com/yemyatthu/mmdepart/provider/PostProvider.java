package com.yemyatthu.mmdepart.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.yemyatthu.mmdepart.data.PostDbHelper;
import com.yemyatthu.mmdepart.data.PostTable;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by yemyatthu on 12/7/14.
 */
public class PostProvider extends ContentProvider {
  public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/posts";
  public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/post";
  private static final int POSTS = 10;
  private static final int POST_ID = 20;

  private static final String AUTHORITY = "com.yemyatthu.mmdepart.provider";

  private static final String BASE_PATH = "posts";

  public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
  private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

  static {
    sURIMatcher.addURI(AUTHORITY, BASE_PATH, POSTS);
    sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", POST_ID);
  }

  private PostDbHelper database;
  private SQLiteDatabase db;

  @Override
  public boolean onCreate() {
    database = new PostDbHelper(getContext());
    db = database.getWritableDatabase();
    return true;
  }

  @Override public Cursor query(Uri uri, String[] projection, String selection,
      String[] selectionArgs, String sortOrder) {
    // Uisng SQLiteQueryBuilder instead of query() method
    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

    // check if the caller has requested a column which does not exists
    checkColumn(projection);

    // Set the table
    queryBuilder.setTables(PostTable.TABLE_NAME);
    int uriType = sURIMatcher.match(uri);
    switch (uriType) {
      case POSTS:
        break;
      case POST_ID:
        queryBuilder.appendWhere(PostTable.COLUMN_ID + " =" + uri.getLastPathSegment());
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    Cursor cursor =
        queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    // make sure that potential listeners are getting notified
    cursor.setNotificationUri(getContext().getContentResolver(), uri);
    return cursor;
  }

  @Override public String getType(Uri uri) {
    return null;
  }

  @Override public Uri insert(Uri uri, ContentValues contentValues) {
    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
    int rowsDeleted = 0;
    long id = 0;
    switch (uriType) {
      case POSTS:
        id = sqLiteDatabase.insert(PostTable.TABLE_NAME, null, contentValues);
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return Uri.parse(BASE_PATH + "/" + id);
  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
    int rowsDeleted = 0;
    switch (uriType) {
      case POSTS:
        rowsDeleted = sqLiteDatabase.delete(PostTable.TABLE_NAME, selection, selectionArgs);
        break;
      case POST_ID:
        String id = uri.getLastPathSegment();
        if (TextUtils.isEmpty(selection)) {
          rowsDeleted =
              sqLiteDatabase.delete(PostTable.TABLE_NAME, PostTable.COLUMN_ID + "=" + id, null);
        } else {
          rowsDeleted = sqLiteDatabase.delete(PostTable.TABLE_NAME,
              PostTable.COLUMN_ID + "=" + id + " and " + selection, selectionArgs);
        }
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return rowsDeleted;
  }

  @Override public int update(Uri uri, ContentValues values, String selection,
      String[] selectionArgs) {

    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
    int rowsUpdated = 0;
    switch (uriType) {
      case POSTS:
        rowsUpdated = sqLiteDatabase.update(PostTable.TABLE_NAME, values, selection, selectionArgs);
        break;
      case POST_ID:
        String id = uri.getLastPathSegment();
        if (TextUtils.isEmpty(selection)) {
          rowsUpdated =
              sqLiteDatabase.update(PostTable.TABLE_NAME, values, PostTable.COLUMN_ID + "=" + id,
                  null);
        } else {
          rowsUpdated = sqLiteDatabase.update(PostTable.TABLE_NAME, values,
              PostTable.COLUMN_ID + "=" + id + " and " + selection, selectionArgs);
        }
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return rowsUpdated;
  }

  public void checkColumn(String[] projection) {
    String[] available = {
        PostTable.COLUMN_LARGE_IMAGE, PostTable.COLUMN_ID, PostTable.COLUMN_SMALL_IMAGE,
        PostTable.COLUMN_LINK, PostTable.COLUMN_MESSAGE, PostTable.COLUMN_OBJECT_ID
    };
    if (projection != null) {
      HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
      HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
      // check if all columns which are requested are available
      if (!availableColumns.containsAll(requestedColumns)) {
        throw new IllegalArgumentException("Unknown columns in projection");
      }
    }
  }
}
