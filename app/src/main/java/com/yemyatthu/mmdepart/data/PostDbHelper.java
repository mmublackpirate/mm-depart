package com.yemyatthu.mmdepart.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by yemyatthu on 12/7/14.
 */
public class PostDbHelper extends SQLiteOpenHelper {
  public static final String DATABASE_NAME = "post.db";
  private static final int DATABASE_VERSION = 1;

  public PostDbHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override public void onCreate(SQLiteDatabase sqLiteDatabase) {
    Log.d("gaaa", "why don't this run?");
    PostTable.onCreate(sqLiteDatabase);
  }

  @Override public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
    PostTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
  }
}
