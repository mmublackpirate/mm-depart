package com.yemyatthu.mmdepart.data;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by yemyatthu on 12/7/14.
 */
public class PostTable implements BaseColumns {

  //Table Name
  public static final String TABLE_NAME = "post";

  //Column Name
  public static final String COLUMN_ID = "_id";
  public static final String COLUMN_MESSAGE = "message";

  //This one is to take the larger size image
  public static final String COLUMN_OBJECT_ID = "object_id";

  public static final String COLUMN_SMALL_IMAGE = "small_image";
  public static final String COLUMN_LARGE_IMAGE = "large_image";
  public static final String COLUMN_LINK = "link";

  // Database creation SQL statement
  private static final String DATABASE_CREATE = "CREATE TABLE "
      + TABLE_NAME
      + "("
      + COLUMN_ID
      + " integer primary key autoincrement, "
      + COLUMN_MESSAGE
      + " text not null, "
      + COLUMN_OBJECT_ID
      + " text not null, "
      + COLUMN_LINK
      + " text not null, "
      + COLUMN_SMALL_IMAGE
      + " text not null, "
      + COLUMN_LARGE_IMAGE
      + " text not null "
      + ");";

  public static void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
  }

  public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
    Log.w(PostTable.class.getName(), "Upgrading database from version "
        + oldVersion
        + " to "
        + newVersion
        + ", which will destroy all old data");
    database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(database);
  }
}
